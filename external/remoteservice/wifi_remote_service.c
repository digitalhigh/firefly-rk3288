/* server.c */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h> 
#include <sys/stat.h> 
#include <fcntl.h>
#include <sys/time.h>

#define MAXLINE 80
#define SERV_PORT 8888

int main(void)
{
	struct sockaddr_in servaddr, cliaddr;
	socklen_t cliaddr_len;
	int sockfd;
	char buf[MAXLINE];
	char str[INET_ADDRSTRLEN];
	int i, n;
	int fd;
	int ret = -1;

	struct timeval tv;
	struct timezone tz;
	long now_sec = 0;
	long now_usec = 0;
	long last_sec = 0;
	long last_usec = 0;
	long time_gap = 0;
	long serialnum = 0;
	long mLastT1 = 0;
	long mTimeStampAll = 0;
	long now = 0;
	long k = 0;

	fd = open("/dev/tchip_rm", O_WRONLY);
	if (fd < 0)
	{
		printf("open /dev/tchip_rm err!\n");
		return -1;
	}

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sockfd < 0)
	{
		printf("socket err!\n");
		return -1;
	}

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(SERV_PORT);
    
	if(bind(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
	{
		printf("lynn bind err!\n");
		return -1;
	}

	printf("Accepting connections ...\n");
	while (1) {
		cliaddr_len = sizeof(cliaddr);
		n = recvfrom(sockfd, buf, MAXLINE, 0, (struct sockaddr *)&cliaddr, &cliaddr_len);
		if (n == -1)
		{
			printf("recvfrom error\n");
			continue;
		}
			
		// printf("wlj received from %s at PORT %d\n",
		//        inet_ntop(AF_INET, &cliaddr.sin_addr, str, sizeof(str)),
		//        ntohs(cliaddr.sin_port));
    
		for(i=0; i<n; i++)
		{
			printf("%c", buf[i]);
		}
		printf("\n");

		gettimeofday (&tv , &tz);
		now_sec = tv.tv_sec;
		now_usec = tv.tv_usec;

#if 0
        if (0 != last_sec)
        {	
        	long tmp = (now_sec - last_sec) * 1000000 + now_usec - last_usec;
        	mTimeStampAll += tmp;
        	k++;
        	//        	if (deltat * 1000 > 50)
//            	{
        		printf("gap:%d AV:%d %d:%d\n", tmp, mTimeStampAll/k, now_sec, now_usec);
//            	}
        	if (tmp > 1000000)
        	{
        		mLastT1 = 0;
        		mTimeStampAll = 0;
        		k = 0;
        	}
        }
        last_sec = now_sec;
        last_usec = now_usec;
#endif
        
		write(fd, buf, n);
		// n = sendto(sockfd, buf, n, 0, (struct sockaddr *)&cliaddr, sizeof(cliaddr));
		// if (n == -1)
		// 	perr_exit("sendto error");
	}

	printf("lynn wifi remote service exit err!\n");
	close(fd);
	return 0;
}
