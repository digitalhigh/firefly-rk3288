LOCAL_PATH:= $(call my-dir)


include $(CLEAR_VARS)
LOCAL_SRC_FILES:=\
	wifi_remote_service.c
LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/builds \
	$(LOCAL_PATH)/include
LOCAL_MODULE:=remoted

include $(BUILD_EXECUTABLE)
