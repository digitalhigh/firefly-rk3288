package com.android.settings;

import java.util.Hashtable;

import android.graphics.Bitmap;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

public final class EncodingHandler {
	private static final int BLACK = 0xff000000;

	public static Bitmap createQRCode(String str,int widthAndHeight) throws WriterException {
		Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();  
		hints.put(EncodeHintType.CHARACTER_SET, "utf-8"); 
		BitMatrix matrix = new MultiFormatWriter().encode(str,
				BarcodeFormat.QR_CODE, widthAndHeight, widthAndHeight);
		matrix = CutWhiteBorder(matrix);
		int width = matrix.getWidth();
		int height = matrix.getHeight();
		int[] pixels = new int[width * height];

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (matrix.get(x, y)) {
					pixels[y * width + x] = BLACK;
				}
			}
		}
		Bitmap bitmap = Bitmap.createBitmap(width, height,
				Bitmap.Config.ARGB_8888);
		bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
		return bitmap;
	}
	private static BitMatrix updateBit(BitMatrix matrix, int margin){
		int tempM = margin*2;
		int[] rec = matrix.getEnclosingRectangle();   //获取二维码图案的属性
		int resWidth = rec[2] + tempM;
		int resHeight = rec[3] + tempM;
		BitMatrix resMatrix = new BitMatrix(resWidth, resHeight); // 按照自定义边框生成新的BitMatrix
		resMatrix.clear();
		for(int i= margin; i < resWidth- margin; i++){   //循环，将二维码图案绘制到新的bitMatrix中
			for(int j=margin; j < resHeight-margin; j++){
				if(matrix.get(i-margin + rec[0], j-margin + rec[1])){
					resMatrix.set(i,j);
				}
			}
		}
		return resMatrix;
	}
	private static BitMatrix CutWhiteBorder(BitMatrix matrix)
	{
	    int[] rec = matrix.getEnclosingRectangle();
	    int resWidth = rec[2] + 1;
	    int resHeight = rec[3] + 1;
	    BitMatrix resMatrix = new BitMatrix(resWidth + 1, resHeight + 1);
	    resMatrix.clear();
	    for (int i = 0; i < resWidth; i++)
	    {
	        for (int j = 0; j < resHeight; j++)
	        {
	            if (matrix.get(i + rec[0], j + rec[1]))
	            {
	                resMatrix.flip(i + 1, j + 1);
	            }
	        }
	    }
	    return resMatrix;
	}
}
