
/* AudioHardwareALSA.cpp
 **
 ** Copyright 2008-2010 Wind River Systems
 **
 ** Licensed under the Apache License, Version 2.0 (the "License");
 ** you may not use this file except in compliance with the License.
 ** You may obtain a copy of the License at
 **
 **     http://www.apache.org/licenses/LICENSE-2.0
 **
 ** Unless required by applicable law or agreed to in writing, software
 ** distributed under the License is distributed on an "AS IS" BASIS,
 ** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 ** See the License for the specific language governing permissions and
 ** limitations under the License.
 */

#include <errno.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <dlfcn.h>

#define LOG_TAG "AudioHardwareALSA"
#include <utils/Log.h>
#include <utils/String8.h>

#include <cutils/properties.h>
#include <media/AudioRecord.h>
#include <hardware_legacy/power.h>

#include "AudioHardwareALSA.h"

extern "C"
{
    //
    // Function for dlsym() to look up for creating a new AudioHardwareInterface.
    //
    android_audio_legacy::AudioHardwareInterface *createAudioHardware(void) {
        return android_audio_legacy::AudioHardwareALSA::create();
    }
}         // extern "C"

extern "C"
{
	#include <cutils/str_parms.h>
}


namespace android_audio_legacy
{

// ----------------------------------------------------------------------------

static void ALSAErrorHandler(const char *file,
                             int line,
                             const char *function,
                             int err,
                             const char *fmt,
                             ...)
{
    char buf[BUFSIZ];
    va_list arg;
    int l;

    va_start(arg, fmt);
    l = snprintf(buf, BUFSIZ, "%s:%i:(%s) ", file, line, function);
    vsnprintf(buf + l, BUFSIZ - l, fmt, arg);
    buf[BUFSIZ-1] = '\0';
    LOG(LOG_ERROR, "ALSALib", "%s", buf);
    va_end(arg);
}

android_audio_legacy::AudioHardwareInterface *AudioHardwareALSA::create() {
    return new AudioHardwareALSA();
}

AudioHardwareALSA::AudioHardwareALSA() :
    mALSADevice(0),
    mAcousticDevice(0),
    mAudioState(0),
    mDirectMode(0),
    mXBMCDirectMode(0),
    mAudioPreprocessing(false)
{
    snd_lib_error_set_handler(&ALSAErrorHandler);
    mMixer = new ALSAMixer;

    hw_module_t *module;
    int err = hw_get_module(ALSA_HARDWARE_MODULE_ID,
            (hw_module_t const**)&module);

    if (err == 0) {
        hw_device_t* device;
        err = module->methods->open(module, ALSA_HARDWARE_NAME, &device);
        if (err == 0) {
            mALSADevice = (alsa_device_t *)device;
            mALSADevice->init(mALSADevice, mDeviceList);
			LOGI("ALSA version is %s", ALSA_VERSION);
        } else
            LOGE("ALSA Module could not be opened!!!");
    } else
        LOGE("ALSA Module not found!!!");

    err = hw_get_module(ACOUSTICS_HARDWARE_MODULE_ID,
            (hw_module_t const**)&module);

    if (err == 0) {
        hw_device_t* device;
        err = module->methods->open(module, ACOUSTICS_HARDWARE_NAME, &device);
        if (err == 0)
            mAcousticDevice = (acoustic_device_t *)device;
        else
            LOGE("Acoustics Module not found.");
    }
	
	mInStream = NULL;

    //mAudioPreprocessing
    char value[PROPERTY_VALUE_MAX] = "";
    property_get("media.audio.preprocessing", value, "");
    if(memcmp(value, "1", 1) == 0){
		LOGI("Enable audio preprocessing.");
        mAudioPreprocessing = true;
    }

	if(mAudioPreprocessing){
	    //agc level init
		property_get("media.audio.agc.level", value, "8000");
		mAgcLevel = atoi(value);
		mAgcLevel = mAgcLevel>32767? 32767:(mAgcLevel<0? 0:mAgcLevel);
		//ns level init
		property_get("media.audio.ns.level", value, "0");
		mNsLevel = atoi(value);
		mNsLevel = mNsLevel>0? 0:(mNsLevel<-60? -60:mNsLevel);

		LOGI("AGC Level: %d; NS Level: %d", mAgcLevel, mNsLevel);
	}

	/* init channel status */
	mChnsta = (unsigned char*)malloc(CHASTA_SUB_NUM);
	if (!mChnsta)
	    LOGE("chnsta malloc failed\n");

	memset(mChnsta, 0x0, CHASTA_SUB_NUM);
	mChnsta[CHASTA_BIT1*2] = 1;
	mChnsta[CHASTA_BIT1*2+1] = 1;
	/* word length default 24 bit*/
	mChnsta[CHASTA_BIT32*2] = 1;
	mChnsta[CHASTA_BIT32*2+1] = 1;
	mChnsta[CHASTA_BIT33*2] = 1;
	mChnsta[CHASTA_BIT33*2+1] = 1;
	mChnsta[CHASTA_BIT35*2] = 1;
	mChnsta[CHASTA_BIT35*2+1] = 1;
	/* sampling frequency default 48k */
	mChnsta[CHASTA_BIT25*2] = 1;
	mChnsta[CHASTA_BIT25*2+1] = 1;
	/* original sampling frequency default 48k */
	mChnsta[CHASTA_BIT36*2] = 1;
	mChnsta[CHASTA_BIT36*2+1] = 1;
	mChnsta[CHASTA_BIT38*2] = 1;
	mChnsta[CHASTA_BIT38*2+1] = 1;
	mChnsta[CHASTA_BIT39*2] = 1;
	mChnsta[CHASTA_BIT39*2+1] = 1;

	/* b, p, c, u, v, 0, 0, 0*/
	for (int i=0; i<CHASTA_SUB_NUM; i++)
	    mChnsta[i] = (mChnsta[i]<<C_BIT_SHIFT) | (0x1<<V_BIT_SHIFT);

	/* B bit */
	mChnsta[CHASTA_BIT0*2] |= (0X1<<B_BIT_SHIFT);
	mChnsta[CHASTA_BIT0*2+1] |= (0X1<<B_BIT_SHIFT);
}

AudioHardwareALSA::~AudioHardwareALSA()
{
    if (mMixer) delete mMixer;
    if (mALSADevice)
        mALSADevice->common.close(&mALSADevice->common);
    if (mAcousticDevice)
        mAcousticDevice->common.close(&mAcousticDevice->common);
    if (mChnsta)
        free(mChnsta);
}

status_t AudioHardwareALSA::initCheck()
{
    if (!mALSADevice)
        return NO_INIT;

    if (!mMixer || !mMixer->isValid())
        LOGW("ALSA Mixer is not valid. AudioFlinger will do software volume control.");

    return NO_ERROR;
}

status_t AudioHardwareALSA::setVoiceVolume(float volume)
{
    // The voice volume is used by the VOICE_CALL audio stream.
    LOGD("AudioHardwareALSA::setVoiceVolume volume = %f",volume);
/*
    if (mMixer)
        return mMixer->setVolume(AudioSystem::DEVICE_OUT_EARPIECE, volume, volume);
    else
        return INVALID_OPERATION;
*/
    if (mOutStream) {
        // the mOutStream will set the volume of current device
        return mOutStream->setVolume(volume, volume);
    } else {
        // return error
        return INVALID_OPERATION;
    }
}

status_t AudioHardwareALSA::setMasterVolume(float volume)
{
    if (mMixer)
        return mMixer->setMasterVolume(volume);
    else
        return INVALID_OPERATION;
}

status_t AudioHardwareALSA::setMode(int mode)
{
	LOGI("AudioHardwareALSA::setMode(): %d\n", mode);
    status_t status = NO_ERROR;
    int pre_mode = AudioHardwareBase::mMode;
	static int debug_time = 0;
	if(mode == AudioSystem::MODE_IN_COMMUNICATION)
		return NO_ERROR;
    if (mode != mMode) {
        status = AudioHardwareBase::setMode(mode);
        if (status == NO_ERROR &&
            mode != AudioSystem::MODE_IN_CALL &&
            mode != AudioSystem::MODE_RINGTONE &&
            pre_mode != AudioSystem::MODE_IN_CALL &&
            pre_mode != AudioSystem::MODE_RINGTONE) {
            // take care of mode change.
            for(ALSAHandleList::iterator it = mDeviceList.begin();it != mDeviceList.end(); ++it) {
                status = mALSADevice->route(&(*it), it->curDev, mode,
                                            (mDirectMode? HW_PARAMS_FLAG_NLPCM : HW_PARAMS_FLAG_LPCM));
                if (status != NO_ERROR)
                    break;
            }
        }
    }

    return status;
}

status_t AudioHardwareALSA::setParameters(const String8& keyValuePairs)
{
    status_t status = NO_ERROR;
	LOGI("AudioHardwareALSA::setParameters() %s", keyValuePairs.string());

    //usb audio case:
	if(memcmp(keyValuePairs.string(),"STREAM",5) == 0){
	    struct str_parms *parms;
	    char value[32];
	    int ret;	
		int card;
		int device;
		int streamType;
		parms = str_parms_create_str(keyValuePairs.string());

		ret = str_parms_get_str(parms, "STREAM", value, sizeof(value));
		if (ret >= 0)
			streamType = atoi(value);
		
		ret = str_parms_get_str(parms, "card", value, sizeof(value));
		if (ret >= 0)
			card = atoi(value);
		
		ret = str_parms_get_str(parms, "device", value, sizeof(value));
		if (ret >= 0)
			device = atoi(value);

        mALSADevice->set_usb_device(streamType, card, device);
		
		str_parms_destroy(parms);

	}

    return status;

}


void AudioHardwareALSA::setnlpcmchnsta()
{
	/* sampling frequency default 48k */
	mChnsta[CHASTA_BIT24*2] &= C_BIT_UNSET;
	mChnsta[CHASTA_BIT24*2+1] &= C_BIT_UNSET;
	mChnsta[CHASTA_BIT25*2] |= C_BIT_SET;
	mChnsta[CHASTA_BIT25*2+1] |= C_BIT_SET;
	mChnsta[CHASTA_BIT26*2] &= C_BIT_UNSET;
	mChnsta[CHASTA_BIT26*2+1] &= C_BIT_UNSET;
	mChnsta[CHASTA_BIT27*2] &= C_BIT_UNSET;
	mChnsta[CHASTA_BIT27*2+1] &= C_BIT_UNSET;
	/* original sampling frequency */
	mChnsta[CHASTA_BIT36*2] |= C_BIT_SET;
	mChnsta[CHASTA_BIT36*2+1] |= C_BIT_SET;
	mChnsta[CHASTA_BIT37*2] &= C_BIT_UNSET;
	mChnsta[CHASTA_BIT37*2+1] &= C_BIT_UNSET;
	mChnsta[CHASTA_BIT38*2] |= C_BIT_SET;
	mChnsta[CHASTA_BIT38*2+1] |= C_BIT_SET;
	mChnsta[CHASTA_BIT39*2] |= C_BIT_SET;
	mChnsta[CHASTA_BIT39*2+1] |= C_BIT_SET;

}

void AudioHardwareALSA::sethbrchnsta()
{
	/* sampling frequency 768k */
	mChnsta[CHASTA_BIT24*2] |= C_BIT_SET;
	mChnsta[CHASTA_BIT24*2+1] |= C_BIT_SET;
	mChnsta[CHASTA_BIT25*2] &= C_BIT_UNSET;
	mChnsta[CHASTA_BIT25*2+1] &= C_BIT_UNSET;
	mChnsta[CHASTA_BIT26*2] &= C_BIT_UNSET;
	mChnsta[CHASTA_BIT26*2+1] &= C_BIT_UNSET;
	mChnsta[CHASTA_BIT27*2] |= C_BIT_SET;
	mChnsta[CHASTA_BIT27*2+1] |= C_BIT_SET;
	/* original sampling frequency */
	mChnsta[CHASTA_BIT36*2] &= C_BIT_UNSET;
	mChnsta[CHASTA_BIT36*2+1] &= C_BIT_UNSET;
	mChnsta[CHASTA_BIT37*2] |= C_BIT_SET;
	mChnsta[CHASTA_BIT37*2+1] |= C_BIT_SET;
	mChnsta[CHASTA_BIT38*2] |= C_BIT_SET;
	mChnsta[CHASTA_BIT38*2+1] |= C_BIT_SET;
	mChnsta[CHASTA_BIT39*2] &= C_BIT_UNSET;
	mChnsta[CHASTA_BIT39*2+1] &= C_BIT_UNSET;
}

void AudioHardwareALSA::dumpchnsta()
{
    for (int i=0; i<CHASTA_SUB_NUM; i=i+16) {
        ALOGI("%02d: 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x "
        "0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x\n",
        i/16, mChnsta[i], mChnsta[i+1], mChnsta[i+2], mChnsta[i+3],
        mChnsta[i+4], mChnsta[i+5], mChnsta[i+6], mChnsta[i+7],
        mChnsta[i+8], mChnsta[i+9], mChnsta[i+10], mChnsta[i+11],
        mChnsta[i+12], mChnsta[i+13], mChnsta[i+14], mChnsta[i+15]);
    }
}

AudioStreamOut *
AudioHardwareALSA::openOutputStream(uint32_t devices,
                                    int *format,
                                    uint32_t *channels,
                                    uint32_t *sampleRate,
                                    status_t *status)
{
    LOGD("openOutputStream called for devices: 0x%08x format %d channels %d sampleRate %d ", devices,
		*format, *channels, *sampleRate);

	char value[PROPERTY_VALUE_MAX] = "";
	property_get("media.cfg.audio.direct",value,"false");
	if(memcmp(value, "true", 4) == 0) {
		mXBMCDirectMode = 1;
		usleep(100*1000);
	} else {
		mXBMCDirectMode = 0;
	}
    if(mAudioState ==1){
        if(mOutStream){
			mOutStream->standby();
        }

		if(devices == AudioSystem::DEVICE_OUT_ANLG_DOCK_HEADSET){
			char value[PROPERTY_VALUE_MAX] = "";
			property_get("media.cfg.audio.bypass", value, "-1");
			if(memcmp(value, "true", 4) == 0){
				mDirectMode = 1;
				usleep(3000 * 1000);
			}
		} else if(devices == AudioSystem::DEVICE_OUT_AUX_DIGITAL){
			char value[PROPERTY_VALUE_MAX] = "";
			property_get("media.cfg.audio.mul", value, "-1");
			if(memcmp(value, "true", 4) == 0){
				mDirectMode = 0;
				usleep(3000 * 1000);
			}else{
				property_get("media.cfg.audio.bypass", value, "-1");
				if(memcmp(value, "true", 4) == 0){
					mDirectMode = 1;
					usleep(3000 * 1000);
				}
			}
		}
    }
    status_t err = BAD_VALUE;
    AudioStreamOutALSA *out = 0;
    uint32_t uChannels = audio_channel_out_count_from_mask(*channels);
    if(uChannels < 2)
        uChannels = 2;

    if (devices & (devices - 1)) {
        if (status) *status = err;
        LOGD("openOutputStream called with bad devices");
        return out;
    }

    // Find the appropriate alsa device
    for(ALSAHandleList::iterator it = mDeviceList.begin();it != mDeviceList.end(); ++it){
		char value[PROPERTY_VALUE_MAX] = "";
		property_get("media.cfg.audio.direct",value,"false");
		if(memcmp(value, "true", 4) == 0) {
			ALOGD("Get media.cfg.audio.direct is true. Set XBMC direct mode! ");
			mXBMCDirectMode = 1;
			++it;
			it->devices = AudioSystem::DEVICE_OUT_AUX_DIGITAL
				      | AudioSystem::DEVICE_OUT_ANLG_DOCK_HEADSET;
		}else {
			mXBMCDirectMode = 0;
		}
        if (it->devices & devices) {
			if(devices == AudioSystem::DEVICE_OUT_ANLG_DOCK_HEADSET){
				char value[PROPERTY_VALUE_MAX] = "";
				property_get("media.cfg.audio.bypass", value, "-1");
				//samplerate passthrough
				//current only accecpt 44100 or 48000				
				if(memcmp(value, "true", 4) == 0){		
					if(*sampleRate==44100 || *sampleRate==48000)
						it->sampleRate = *sampleRate;
					else
						LOGE("direct sampleRate %d unsupported at the time", *sampleRate);
					mDirectMode=1;
				}
			} else if(devices == AudioSystem::DEVICE_OUT_AUX_DIGITAL){
                char value[PROPERTY_VALUE_MAX] = "";
                property_get("media.cfg.audio.mul", value, "-1");
                if(memcmp(value, "true", 4) == 0){		
                    if(*sampleRate==44100 || *sampleRate==48000){
                        it->sampleRate = *sampleRate;
                    }else{
                        LOGE("hdmi 5.1 sampleRate %d unsupported at the time", *sampleRate);
                        it->sampleRate = 44100;
                    }
                    mDirectMode=0;
                }else {
	                property_get("media.cfg.audio.bypass", value, "-1");
	                if(memcmp(value, "true", 4) == 0){		
                        if(*sampleRate==44100 || *sampleRate==48000 || *sampleRate==192000){
                            it->sampleRate = *sampleRate;
                        }else{
                            LOGE("hdmi passthrough sampleRate %d unsupported at the time", *sampleRate);
                            it->sampleRate = 44100;
                        }
                        mDirectMode=1;
                    }
                }
            } else {
                it->sampleRate = 44100;
            }
            it->channels = uChannels;

            int flag=HW_PARAMS_FLAG_LPCM;
            if(mDirectMode)
                flag=HW_PARAMS_FLAG_NLPCM;

            bool bitstrm = false;
            bool ishbr = false;
            if(HW_PARAMS_FLAG_NLPCM == flag && devices == AudioSystem::DEVICE_OUT_AUX_DIGITAL){
                LOGI("bitstream mode\n");
                it->format = SND_PCM_FORMAT_S24_3LE;
                bitstrm = true;
                if(192000 == it->sampleRate && 8 == it->channels){
                    /* modify chnsta for hbr */
                    ishbr = true;
                    sethbrchnsta();
                }else{
                    setnlpcmchnsta();
                }
                /* dumpchnsta(); */
            }else{
                it->format = SND_PCM_FORMAT_S16_LE;
                bitstrm = false;
            }

            err = mALSADevice->open(&(*it), devices, mode(), flag);
            if (err) break;
            out = new AudioStreamOutALSA(this, &(*it));
			out->mCurDev = devices;
			out->mBitstreamMode = bitstrm;
			out->mIsHbr = ishbr;
			mOutStream = out;
            err = out->set(format, channels, sampleRate);
            break;
        }
    }

    if (status) *status = err;
    return out;
}

void
AudioHardwareALSA::closeOutputStream(AudioStreamOut* out)
{
	LOGE("AudioHardwareALSA::closeOutputStream");
    delete out;
	mOutStream = NULL;
}

AudioStreamIn *
AudioHardwareALSA::openInputStream(uint32_t devices,
                                   int *format,
                                   uint32_t *channels,
                                   uint32_t *sampleRate,
                                   status_t *status,
                                   AudioSystem::audio_in_acoustics acoustics)
{
	LOGD("openInputStream called for devices: 0x%08x", devices);
    status_t err = BAD_VALUE;
    AudioStreamInALSA *in = 0;

    if (devices & (devices - 1)) {
        if (status) *status = err;
        return in;
    }

    // Find the appropriate alsa device
    for(ALSAHandleList::iterator it = mDeviceList.begin();
        it != mDeviceList.end(); ++it)
        if (it->devices & devices) {            
            //set samplerate to alsa_lib, do not use AudioResampler in framework
            uint32_t rate = AudioStreamInALSA::getInputSampleRate(*sampleRate);
            if (rate != *sampleRate) {
				LOGE("unsupported samplerate:%d\n", *sampleRate);
                *sampleRate = rate;
                err = BAD_VALUE;
                break;
            }
            it->sampleRate = *sampleRate;

			it->periodSize = (snd_pcm_uframes_t)AudioStreamInALSA::getInputBufferSize(*sampleRate,*format,1)/sizeof(int16_t)/2;// frame
			it->bufferSize = it->periodSize * it->periods;// frame			
            err = mALSADevice->open(&(*it), devices, mode(), HW_PARAMS_FLAG_LPCM);
            if (err) break;
            in = new AudioStreamInALSA(this, &(*it), acoustics);
            err = in->set(format, channels, sampleRate);
            mInStream = in;
            break;
        }

    if (status) *status = err;
    return in;
}

void
AudioHardwareALSA::closeInputStream(AudioStreamIn* in)
{
    delete in;
	mInStream = NULL;
}

size_t    AudioHardwareALSA::getInputBufferSize(uint32_t sampleRate, int format, int channels)
{
	LOGD("AudioHardwareALSA::getInputBufferSize sampleRate:%d,format:%d,channelCount:%d",
		sampleRate,format,channels);
	return AudioStreamInALSA::getInputBufferSize(sampleRate,format,channels);
}

status_t AudioHardwareALSA::setMicMute(bool state)
{
    if (mInStream)
        return mInStream->setMicMute(state);
	else if (mMixer)
		return mMixer->setCaptureMuteState(AudioSystem::DEVICE_IN_ALL, state);		
			
    return NO_INIT;
}

status_t AudioHardwareALSA::getMicMute(bool *state)
{
    if (mInStream)
        return mInStream->getMicMute(state);	
	else if (mMixer)
		return mMixer->getCaptureMuteState(AudioSystem::DEVICE_IN_ALL, state);	
			
    return NO_ERROR;
}

status_t AudioHardwareALSA::dump(int fd, const Vector<String16>& args)
{
    return NO_ERROR;
}

#ifdef ALSA_LRSWITCH
void AudioHardwareALSA::setDtvOutputMode(int mode)
{
       if(mOutStream){
               mOutStream->setDtvOutputMode(mode);
       }
}
#endif

}       // namespace android
